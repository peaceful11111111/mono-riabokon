function sumFormat (sum) {
  let formatter = new Intl.NumberFormat("ru", {maximumFractionDigits: 2, minimumFractionDigits: 2});

  return formatter.format(sum).replace(',', '.');
}

const input = document.getElementById("sum");


class inputCurrency {
  constructor(input) {
    this.input = input;
    this.regex = /^[1-9][0-9]*(\.)?\d{0,2}$/;
    this.viewVal = '';
    this.prevVal = '';
    this._events();
  }

  _events() {
    this.input.addEventListener('input', this._processInput.bind(this));
    this.input.addEventListener('blur', this._polishInput.bind(this));
  }

  _polishInput() {
    const [integer, digits = ''] = this.input.value.split('.');
    if (!integer) return;
    this.input.value = `${integer}.${digits.padEnd(2, '0')}`;
  }

  _processInput() {
    const value = this.input.value.replace(/ /g, '');
    if (!value) {
      this.prevVal = '';
      return;
    }

    if (!this.regex.test(value)) {
      console.log('fail');
      this.input.value = this.prevVal;
    } else {
      this.viewVal = this._addSpaces(value);
      this.prevVal = this.viewVal;
      this.input.value = this.viewVal;
    }
  }

  _addSpaces(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
  }
}

new inputCurrency(input);
